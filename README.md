# Symetris: Drupal PHPCS

A PHPCS coding standard for Drupal 8+ installations.

## Installation

For installing this package, add these lines to the project's `composer.json`.

```json
{
  "extra": {
    "drupal-scaffold": {
      "allowed-packages": [
        "symetris/drupal-phpcs"
      ]
    }
  },
  "repositories": [
    {
      "type": "git-bitbucket",
      "url": "https://bitbucket.org/symetris/drupal-phpcs"
    }
  ]
}
```

The package can be installed like a regular composer package as follows.

### Install a branch

    composer require symetris/drupal-phpcs:1.x-dev

### Install a tag

    composer require symetris/drupal-phpcs:1.0.0-alpha1

### docroot

If your Drupal document root is `docroot` instead of `web`, you'll need to
modify the `phpcs.xml.dist` file to rename all instances of `web/` to `docroot/`.

### Required directories

PHPCS is strict about the directories it needs to scan. Thus, the following
directories must exist in the project:

    modules/custom
    themes/custom
    profiles/custom
    scripts

You can ensure these directories exist in your project by creating an empty
file named `.gitkeep` in those directories and committing them.

## How it works

This package depends on `drupal/core-composer-scaffold` to install a
`phpcs.xml.dist` file in the root of the Drupal project. You should track
this file in Git.

## Extending

If you need to add, edit, or remove certain coding standard rules for your
project, you should create a `phpcs.xml` file in your project's root based
on the `phpcs.xml.dist` created by this package. Then, in this file, you
can use the `DrupalSymetris` ruleset and modify it as per your requirements.
